import os
import flask
import requests

app = flask.Flask(__name__)

def to_bool(value):
    return str(value).lower() in ('t', 'true', '1', 'y', 'yes')


FLASK_PROXY_HOST = os.environ.get("FLASK_PROXY_HOST", "192.168.1.121")
FLASK_PROXY_PORT = os.environ.get("FLASK_PROXY_PORT", 5555)
FLASK_DEBUG_MODE = to_bool(os.environ.get("FLASK_DEBUG_MODE", False))
FLASK_REDIRECT_HOST = os.environ.get("FLASK_REDIRECT_HOST", "https://localhost:8443")
HOST_EMAIL = os.environ.get("HOST_EMAIL", "admin@soundgram.co")
HOST_PASSWORD = os.environ.get("HOST_PASSWORD", "admin123!")
HOST_LOGIN_URL = os.environ.get("HOST_LOGIN_URL", "/accounts/login/?next=/")

request_methods_mapping = {
    'GET': requests.get,
    'HEAD': requests.head,
    'POST': requests.post,
    'PUT': requests.put,
    'DELETE': requests.delete,
    'PATCH': requests.patch,
    'OPTIONS': requests.options,
}


def try_login():
    root_url = f"{FLASK_REDIRECT_HOST}/broadcast/admin/login/"
    login_url = f"{FLASK_REDIRECT_HOST}/{HOST_LOGIN_URL}"

    session = requests.Session()

    headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
    }

    r = session.get(
        root_url,
        stream=True,
        verify=False,
        headers=headers,
    )

    print(f"\nSend request to: {root_url}")

    if "csrftoken" in session.cookies.get_dict():
        csrftoken = session.cookies.get_dict()["csrftoken"]
    else:
        csrftoken = session.cookies.get_dict()["csrf"]

    return csrftoken

@app.route("/<path:route>", methods=request_methods_mapping.keys())
def proxy(route):
    requests_function = request_methods_mapping[flask.request.method]
    requests_function_name = requests_function.__name__.upper()
    request_is_json = flask.request.is_json
    files = flask.request.files

    for file in files:
        print(f"\nFile: {file}")

    print(f"\nProxy request '{requests_function_name}' to route '{route}' is json '{request_is_json}'")

    headers = {}

    if flask.request.headers:
        for key in flask.request.headers.keys():
            headers[key] = flask.request.headers[key]

    if 'Host' in headers:
        headers.pop('Host', None)

    if 'Origin' in headers:
        headers.pop('Origin', None)

    payload=None

    if request_is_json:
        payload = flask.request.get_json()

    if 'favicon.ico' in route:
        return flask.Response(response='favicon.ico', mimetype='text/plain', status=200)

    url = f"{FLASK_REDIRECT_HOST}/{route}"

    print(f"\nProxy redirect to '{url}'")

    csrftoken = try_login()

    headers["Referer"] = url
    headers["csrfmiddlewaretoken"] = csrftoken

    print(f"\nSet headers for redirected request as: {headers}")

    try:
        if payload is None:
            request = requests_function(
                url,
                stream=True,
                verify=False,
                params=flask.request.args,
                headers=headers,
                files=files,
            )
        else:

            request = requests_function(
                url,
                stream=True,
                verify=False,
                params=flask.request.args,
                headers=headers,
                files=files,
                json=payload,
            )
        response = flask.Response(
            flask.stream_with_context(request.iter_content()),
            content_type=request.headers['content-type'],
            status=request.status_code
        )
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response

    except Exception as ex:
        print(f"Got an exception: {ex}")
        return flask.Response(response='Service unreachable', mimetype='text/plain', status=500)

if __name__ == '__main__':
    app.debug = FLASK_DEBUG_MODE,
    app.run(
        host=FLASK_PROXY_HOST,
        port=FLASK_PROXY_PORT
    )