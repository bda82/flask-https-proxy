#!/bin/bash

export FLASK_PROXY_HOST=localhost
export FLASK_PROXY_PORT=5555
export FLASK_DEBUG_MODE=True
export FLASK_REDIRECT_HOST=https://localhost:8443
export HOST_EMAIL=admin@soundgram.com
export HOST_PASSWORD=admin123!
export HOST_LOGIN_URL=/accounts/login/?next=/

python proxy.py