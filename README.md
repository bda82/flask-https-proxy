# flask-https-proxy

1. Set env variables in file run.sh

2. Run ngrok like this:

```sh
./ngrok http 5555
```

3. Copy shared link like this

```sh
http://655cfdfc7883.ngrok.io
```

4. Run proxy like this

```sh
./run.sh
```

5. Use links like this from local machine

```sh
http://0.0.0.0:5555/broadcast/graphql
```

for using

```sh
https://0.0.0.0:8443/broadcast/graphql/
```

6. Use links like this from external machine

```sh
http://655cfdfc7883.ngrok.io/broadcast/graphql/
```

for using

```sh
https://0.0.0.0:8443/broadcast/graphql/
```